package faust.providence.app.simplelistdemo

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    /**
     * Constants
     */
    companion object {
        val RES = "resource"
        val MODE = "mode"
        val SELECTED = "selected"
        val REQUEST_CODE = 1
    }

    /**
     * Show the list view with android layout (android.R.layout.simple_...)
     */
    fun show(view: View) {
        val intent = Intent(this, ListActivity::class.java)
        when(opLayout.selectedItemPosition) {
            0 -> intent.putExtra(RES,android.R.layout.simple_list_item_1)
            1 -> intent.putExtra(RES,android.R.layout.simple_list_item_checked)
            2 -> intent.putExtra(RES,android.R.layout.simple_list_item_multiple_choice)
            3 -> intent.putExtra(RES,android.R.layout.simple_list_item_single_choice)
            4 -> intent.putExtra(RES,android.R.layout.simple_list_item_2)
        }

        val mode = this.opMode.selectedItem.toString()
        when {
            mode.contains("1") -> intent.putExtra(MODE,1)
            mode.contains("2") -> intent.putExtra(MODE,2)
            mode.contains("3") -> intent.putExtra(MODE,3)
            else -> intent.putExtra(MODE,0)
        }

        this.startActivity(intent)
    }

    /**
     * Expandable list view, use android.R.layout.simple_expandable_item_1 & .simple_expandable_item_2
     */
    fun expandable(view: View) {
        val intent = Intent(this,ExpandableActivity::class.java)
        this.startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_CODE) {
            if(data!= null && data.hasExtra(SELECTED)) {
                this.txtSelected.text = data.extras.getString(SELECTED)
            } else {
                this.txtSelected.text = "(Item selected)"
            }

        }
    }
}
