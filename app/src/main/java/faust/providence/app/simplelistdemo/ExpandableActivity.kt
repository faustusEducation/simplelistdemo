package faust.providence.app.simplelistdemo

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.SimpleExpandableListAdapter
import android.widget.ExpandableListView
import kotlinx.android.synthetic.main.activity_main.view.*

class ExpandableActivity : AppCompatActivity() {

    /**
     * Original data for expandable_list
     */
    companion object {
        private val NAME = "NAME"
        private val GROUP = arrayOf<String>("Veggies (vegetables)", "Fruits")
        private val CHILD = arrayOf<Array<String>>(
            arrayOf<String>("Carrot(紅蘿蔔)", "Cucumber(黃瓜)", "Beet(甜菜)"),
            ListActivity.titles
        )
    }

    /**
     * redefine the view component assigning to ExpandablelistView
     * and must assign the physical body by fineViewById(R.id...)
     */
    internal var expandableListView: ExpandableListView? = null

    internal var selected: String? = null
    internal var index: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expandable)

        // the sturcture is
        // ArrayList of ...          ArrayList of ...
        // Map("NAME",Group1Name) -> List of ...
        //                        -> Map("NAME",Group1Child1Name)
        //                        -> Map("NAME",Group1Child2Name)
        //                        -> Map("NAME",Group1Child3Name)
        //                        -> ...
        // Map("NAME",Group2Name) -> List of ...
        //                        -> Map("NAME",Group2Child1Name)
        //                        -> Map("NAME",Group2Chidl2Name)
        //                        -> Map("NAME",Group2Child3Name)
        //                        -> ...
        val groupData = ArrayList<Map<String, String>>()
        val childData = ArrayList<List<Map<String, String>>>()
        for (i in GROUP.indices) {
            val map = HashMap<String, String>()
            groupData.add(map)
            map.put(NAME, GROUP[i])

            val children = ArrayList<Map<String, String>>()
            for (j in 0..CHILD[i].size - 1) {
                val childMap = HashMap<String, String>()
                children.add(childMap)
                childMap.put(NAME, CHILD[i][j])
            }
            childData.add(children)
        }
        val adapter = SimpleExpandableListAdapter(
            this, groupData,
            android.R.layout.simple_expandable_list_item_1,
            arrayOf<String>(NAME), intArrayOf(android.R.id.text1),
            childData, android.R.layout.simple_expandable_list_item_2,
            arrayOf<String>(NAME), intArrayOf(android.R.id.text1)
        )

        // this line is requried while we define the view component by myself, not by graph tool
        // Check there is internal expandableListView declation
        this.expandableListView = findViewById(R.id.expandableListView)

        // In the listView, use listView.adapter = ..., but use .setAdapter(...) here
        this.expandableListView!!.setAdapter(adapter as ExpandableListAdapter)

        this.expandableListView!!.setOnChildClickListener { expandableListView, view, i, j, l ->
            this.index += 1
            if(this.selected == null) {
                this.selected = this.index.toString()+"."+GROUP[i]+": "+ CHILD[i][j] + "\r\n"
            } else {
                this.selected += this.index.toString()+"."+GROUP[i] + ": " + CHILD[i][j] + "\r\n"
            }
            true }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra(MainActivity.SELECTED, this.selected )
        this.setResult(Activity.RESULT_OK,intent)
        // must put at the end, in this case, this.finish() works too.
        super.onBackPressed()
    }
}
