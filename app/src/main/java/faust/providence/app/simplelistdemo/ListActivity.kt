package faust.providence.app.simplelistdemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {

    companion object {
        val titles = arrayOf(
            "fruit",
            "apple",
            "orange",
            "water melon",
            "tomato",
            "guava",
            "banana",
            "pine",
            "durian",
            "mangosteen",
            "mango",
            "pear",
            "cherry",
            "strawberry"
        )
        val text = arrayOf("水果", "蘋果", "橘子", "西瓜", "馬鈴薯", "番石榴(芭樂)", "香蕉", "鳯梨", "榴連", "山竹", "芒果", "梨", "櫻桃", "草莓")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        val resourceID = this.intent.extras.getInt(MainActivity.RES)

        when(resourceID) {
             android.R.layout.simple_list_item_2 -> {
                val lists = mutableListOf<HashMap<String, String>>()
                //使用List存入HashMap，用來顯示ListView上面的文字。
                for (i in titles.indices) {
                    val hashMap = HashMap<String, String>()
                    hashMap.put("title", titles[i])
                    hashMap.put("text", text[i])
                    //把title , text存入HashMap之中
                    lists.add(hashMap)
                    //把HashMap存入list之中
                }
                val listAdapter = SimpleAdapter(
                    this,
                    lists,
                    android.R.layout.simple_list_item_2,
                    arrayOf("title", "text"),
                    intArrayOf(android.R.id.text1, android.R.id.text2)
                )
                // 5個參數 : context , List , layout , key1 & key2 , text1 & text2
                listView.adapter = listAdapter
            }
            else -> {
                val mode = this.intent.extras.getInt(MainActivity.MODE)

                this.listView.choiceMode = mode
                this.listView.adapter = ArrayAdapter<String>(this, resourceID, titles)
            }
        }
    }

}
